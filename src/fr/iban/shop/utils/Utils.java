package fr.iban.shop.utils;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import fr.iban.shop.Main;
import fr.iban.shop.Main.ShopType;
import fr.iban.shop.data.PlayerData;
import fr.iban.shop.data.ShopData;

public class Utils {
	
	public static HashMap<Integer, ShopData> ShopMap = Main.ShopMap;
	public static HashMap<String, PlayerData> PlayerMap = Main.PlayerMap;
	
	public static ItemStack ItemBuilder(int id){
		ShopData data = ShopMap.get(id);
		Material material = data.getMaterial();
		double achat = data.getValue() * 10 * data.getChange();
		achat = (double)((int)(achat*100))/100;
		double vente = data.getValue() * data.getChange();
		vente = (double)((int)(vente*100))/100;
		short damage = (short)data.getDamage();
		ItemStack it = new ItemStack(material, 1 , (short) damage);
		ItemMeta im = it.getItemMeta();
		ArrayList<String> description = new ArrayList<String>();
		description.add("§cAchat : §b" + achat + "$§7 (clic gauche)");
		if(data.getType().equals(ShopType.ITEM)){
			description.add("§aVente : §b" + vente + "$§7 (clic droit)");
			description.add("§7Clic molette pour tout vendre.");
		}else if(data.getType().equals(ShopType.SPAWNER)){
			im.setDisplayName("§a§lSpawner à " + data.getExtras().getMobName());
		}
		
		im.setLore(description);
		it.setItemMeta(im);
		return it;
	}
	
	public static int getItemAtSlot(int slot){
		int id = 0;
		for(int i : ShopMap.keySet()){
			if(ShopMap.get(i).getCategorie().equals("Spawners")){
				if(ShopMap.get(i).getSlot() == slot){
					id = i;
				}
			}
		}
		return id;
	}
	
	public static void setItemsByCategories(Inventory inv, String categorie){
		inv.setItem(0, new ItemStack(Material.STAINED_GLASS_PANE, 1, (short)3));
		inv.setItem(8, new ItemStack(Material.STAINED_GLASS_PANE, 1, (short)3));
		inv.setItem(9, new ItemStack(Material.STAINED_GLASS_PANE, 1, (short)3));
		inv.setItem(17, new ItemStack(Material.STAINED_GLASS_PANE, 1, (short)3));
		inv.setItem(18, new ItemStack(Material.STAINED_GLASS_PANE, 1, (short)3));
		inv.setItem(26, new ItemStack(Material.STAINED_GLASS_PANE, 1, (short)3));
		inv.setItem(27, new ItemStack(Material.STAINED_GLASS_PANE, 1, (short)3));
		inv.setItem(35, new ItemStack(Material.STAINED_GLASS_PANE, 1, (short)3));
		inv.setItem(36, new ItemStack(Material.STAINED_GLASS_PANE, 1, (short)3));
		inv.setItem(44, new ItemStack(Material.STAINED_GLASS_PANE, 1, (short)3));
		inv.setItem(45, new ItemStack(Material.STAINED_GLASS_PANE, 1, (short)3));
		inv.setItem(46, new ItemStack(Material.STAINED_GLASS_PANE, 1, (short)3));
		inv.setItem(47, new ItemStack(Material.STAINED_GLASS_PANE, 1, (short)3));
		inv.setItem(48, new ItemStack(Material.STAINED_GLASS_PANE, 1, (short)3));
		inv.setItem(49, new ItemStack(Material.STAINED_GLASS_PANE, 1, (short)3));
		inv.setItem(50, new ItemStack(Material.STAINED_GLASS_PANE, 1, (short)3));
		inv.setItem(51, new ItemStack(Material.STAINED_GLASS_PANE, 1, (short)3));
		inv.setItem(52, new ItemStack(Material.STAINED_GLASS_PANE, 1, (short)3));
		inv.setItem(53, new ItemStack(Material.STAINED_GLASS_PANE, 1, (short)3));
		for(int i : ShopMap.keySet()){
			ShopData data = ShopMap.get(i);
			if(ShopMap.get(i).getCategorie().equals(categorie)){
				inv.setItem(data.getSlot(), ItemBuilder(i));
			}
		}
	}
}
