package fr.iban.shop.data;

import org.bukkit.Material;
import fr.iban.shop.Main.ShopType;

public class ShopData {
	
	private int shopid;
	private String categorie;
	private int slot;
	private Material material;
	private int damage;
	private int value;
	private int ventes;
	private int achats;
	private ShopType type;
	private ExtraData extras;
	
	
	public ShopData(int shopid, String categorie, int slot, Material material, int i, int value, ShopType type){
		this.shopid = shopid;
		this.categorie = categorie;
		this.slot = slot;
		this.material = material;
		this.damage = i;
		this.value = value;
		this.setType(type);
	}


	public int getShopid() {
		return shopid;
	}


	public void setShopid(int shopid) {
		this.shopid = shopid;
	}


	public String getCategorie() {
		return categorie;
	}


	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}


	public int getSlot() {
		return slot;
	}


	public void setSlot(int slot) {
		this.slot = slot;
	}


	public Material getMaterial() {
		return material;
	}


	public void setMaterial(Material material) {
		this.material = material;
	}


	public int getDamage() {
		return damage;
	}


	public void setDamage(int damage) {
		this.damage = damage;
	}


	public int getValue() {
		return value;
	}


	public void setValue(int value) {
		this.value = value;
	}


	public int getAchats() {
		return achats;
	}


	public void setAchats(int achats) {
		this.achats = achats;
	}


	public int getVentes() {
		return ventes;
	}


	public void setVentes(int ventes) {
		this.ventes = ventes;
	}


	public double getChange() {
		double VChange = -((double)getVentes()/100)/100;
		double BChange = ((double)getAchats()/10)/100;
		VChange = (double)((int)(VChange*100))/100;
		BChange = (double)((int)(BChange*100))/100;
		double TChange = VChange + BChange;
		if(TChange > 0.8){
			TChange = 0.8;
		}
		if(TChange < -0.8){
			TChange = -0.8;
		}
		return 1 + TChange;
	}


	public ShopType getType() {
		return type;
	}


	public void setType(ShopType type) {
		this.type = type;
	}


	public ExtraData getExtras() {
		return extras;
	}


	public void setExtras(ExtraData extras) {
		this.extras = extras;
	}


}
