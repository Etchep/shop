package fr.iban.shop.data;

import org.bukkit.inventory.ItemStack;

public class ExtraData {
	
	private ItemStack itemstack;
	private String Command;
	private String mob;
	private String mobName;
	
	public ExtraData(ItemStack item, String cmd, String mob, String mobName){
		if(item != null){
			this.itemstack = item;
		}
		if(cmd != null){
			this.Command = cmd;
		}
		if(mob != null){
			this.mob = mob;
		}
		if(mobName != null){
			this.setMobName(mobName);
		}
	}

	public String getCommand() {
		return Command;
	}

	public void setCommand(String command) {
		Command = command;
	}

	public ItemStack getItemstack() {
		return itemstack;
	}

	public void setItemstack(ItemStack itemstack) {
		this.itemstack = itemstack;
	}

	public String getMob() {
		return mob;
	}

	public void setMob(String mob) {
		this.mob = mob;
	}

	public String getMobName() {
		return mobName;
	}

	public void setMobName(String mobName) {
		this.mobName = mobName;
	}

}
