package fr.iban.shop.data;

public class PlayerData {
	
	private int CurrentItem;
	private int amount;
	private int type;
	
	public PlayerData(int CurrentItem, int amount, int type){
		this.CurrentItem = CurrentItem;
		this.amount = amount;
		this.setType(type);
	}

	public int getCurrentItem() {
		return CurrentItem;
	}

	public void setCurrentItem(int currentItem) {
		CurrentItem = currentItem;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		if(amount > 1){
			if(amount <= 64){
				this.amount = amount;
			}else{
				this.amount = 64;
			}
		}else{
			this.amount = 1;
		}
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
	
	
	

}
