package fr.iban.shop;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.iban.shop.GUIs.Menu;

public class Commands implements CommandExecutor {

	@Override
	  public boolean onCommand(CommandSender sender, Command arg1, String label, String[] args)
	  {
	    if ((sender instanceof Player)) {
	      Player p = (Player)sender;
	      if (label.equalsIgnoreCase("shop"))
	      p.openInventory(Menu.Menu);
	    }
	    return false;
	  }

}
