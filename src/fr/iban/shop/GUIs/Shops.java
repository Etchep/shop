package fr.iban.shop.GUIs;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import fr.iban.shop.Main;
import fr.iban.shop.Main.ShopType;
import fr.iban.shop.data.ShopData;
import fr.iban.shop.utils.Utils;

public class Shops implements Listener {

	public static HashMap<Integer, ShopData> ShopMap = Main.ShopMap;

	public static Inventory Agriculture = Bukkit.createInventory(null, 54, "�dAgriculture");
	public static Inventory Minerais = Bukkit.createInventory(null, 54, "�dMinerais");
	public static Inventory Blocks = Bukkit.createInventory(null, 54, "�dBlocs");
	public static Inventory Loots = Bukkit.createInventory(null, 54, "�dLoots des mobs");
	public static Inventory Nourriture = Bukkit.createInventory(null, 54, "�dNourriture");
	public static Inventory Pillage = Bukkit.createInventory(null, 54, "�dPillage");
	public static Inventory Spawners = Bukkit.createInventory(null, 54, "�dSpawners");

	private static ItemStack getItemBack() {
		ItemStack i = new ItemStack(new ItemStack(Material.NETHER_STAR, 1));
		ItemMeta im = i.getItemMeta();
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("�6�lRetour au menu");
		description.add(ChatColor.YELLOW + "");
		description.add("�a�lClic pour retourner au menu !");
		i.setItemMeta(im);
		return i;
	}

	@EventHandler
	public void onOpen(InventoryOpenEvent e){
		Inventory inv = e.getInventory();
		if(inv.getName().equalsIgnoreCase(Agriculture.getName())){
			Utils.setItemsByCategories(inv, "Agriculture");
			inv.setItem(49, getItemBack());
		}
		if(inv.getName().equalsIgnoreCase(Minerais.getName())){
			Utils.setItemsByCategories(inv, "Minerais");
			inv.setItem(49, getItemBack());
		}
		if(inv.getName().equalsIgnoreCase(Loots.getName())){
			Utils.setItemsByCategories(inv, "Loots");
			inv.setItem(49, getItemBack());
		}
		if(inv.getName().equalsIgnoreCase(Blocks.getName())){
			Utils.setItemsByCategories(inv, "Blocks");
			inv.setItem(49, getItemBack());
		}
		if(inv.getName().equalsIgnoreCase(Spawners.getName())){
			Utils.setItemsByCategories(inv, "Spawners");
			inv.setItem(49, getItemBack());
		}
		if(inv.getName().equalsIgnoreCase(Nourriture.getName())){
			Utils.setItemsByCategories(inv, "Nourriture");
			inv.setItem(49, getItemBack());
		}
		if(inv.getName().equalsIgnoreCase(Pillage.getName())){
			Utils.setItemsByCategories(inv, "Pillage");
			inv.setItem(49, getItemBack());
		}
	}

	@EventHandler
	public void onInteract(InventoryClickEvent e){
		Player p = (Player)e.getWhoClicked();
		Inventory inv = e.getInventory();
		ItemStack current = e.getCurrentItem();

		if(current == null) return;
		if(inv.getName().equalsIgnoreCase(Agriculture.getName())
				|| inv.getName().equalsIgnoreCase(Minerais.getName())
				|| inv.getName().equalsIgnoreCase(Blocks.getName())
				|| inv.getName().equalsIgnoreCase(Spawners.getName())
				|| inv.getName().equalsIgnoreCase(Nourriture.getName())
				|| inv.getName().equalsIgnoreCase(Pillage.getName())
				|| inv.getName().equalsIgnoreCase(Loots.getName())){
			if(e.getSlot() == 49){
				p.openInventory(Menu.Menu);
			}
			for(int i : ShopMap.keySet()){
				if(current.getType().equals(ShopMap.get(i).getMaterial())){
					if(ShopMap.get(i).getType().equals(ShopType.SPAWNER)){
						int id = Utils.getItemAtSlot(e.getSlot());
						ConfirmInventory.openInv(0, id, p);
					}
					if(ShopMap.get(i).getType().equals(ShopType.ITEM)){
						if(e.getClick().isRightClick()){
							ConfirmInventory.openInv(1, i, p);
						}
						if(e.getClick().isLeftClick()){
							ConfirmInventory.openInv(0, i, p);
						}
						if(e.getClick() == ClickType.MIDDLE){
							ShopData data = ShopMap.get(i);
							double prix = data.getValue() * data.getChange();
							prix = (double)((int)(prix*100))/100;
							int amount = 0;
							for(ItemStack item : p.getInventory().getContents()){
								if(item != null){
									if(item.getType().equals(current.getType())){
										amount = amount + item.getAmount();
										p.getInventory().remove(item);
									}
								}
							}
							if(amount > 0){
								prix = prix*amount;
								Main.getEcon().depositPlayer(p, prix);
								p.sendMessage("�aVous avez vendu " + amount + " " + data.getMaterial().toString() + " pour " + prix + "$ ." );
							}else{
								p.sendMessage("�cVous ne possedez aucun " + data.getMaterial().toString() + ".");
							}	

						}
					}
				}
				e.setCancelled(true);
			}
		}


	}

}
