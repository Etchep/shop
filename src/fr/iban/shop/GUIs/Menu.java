package fr.iban.shop.GUIs;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;


public class Menu implements Listener{
	
	
	public static Inventory Menu = Bukkit.createInventory(null, 9, "�d�lMenu du shop");

	public ItemStack getItem(Material material, String name, int quantity, String desc){
		ItemStack it = new ItemStack(material, quantity);
		ItemMeta itM = it.getItemMeta();
		if(name != null )itM.setDisplayName(name);
		if(desc != null) itM.setLore(Arrays.asList(desc));
		it.setItemMeta(itM);
		return it;
	}
	@EventHandler
	public void onOpen(InventoryOpenEvent e1){
		Inventory inv = e1.getInventory();
		if(inv.getName().equalsIgnoreCase(Menu.getName())){
			Menu.setItem(1, getItem(Material.WHEAT, "�a�lAgriculture", 1, "�dClic pour y acc�der !"));
			Menu.setItem(2, getItem(Material.GOLD_INGOT, "�a�lMinerais", 1, "�dClic pour y acc�der !"));
			Menu.setItem(3, getItem(Material.GRASS, "�a�lBlocs", 1, "�dClic pour y acc�der !"));
			Menu.setItem(4, getItem(Material.ROTTEN_FLESH, "�a�lLoots de mobs", 1, "�dClic pour y acc�der !"));
			Menu.setItem(5, getItem(Material.TNT, "�a�lPillage", 1, "�dClic pour y acc�der !"));
			Menu.setItem(6, getItem(Material.MOB_SPAWNER, "�a�lSpawners", 1, "�dClic pour y acc�der !"));
			Menu.setItem(7, getItem(Material.COOKED_CHICKEN, "�a�lNourriture", 1, "�dClic pour y acc�der !"));
			}
		}
	
	@EventHandler
	public void onInteract(InventoryClickEvent e){
		 Player p = (Player)e.getWhoClicked();
		 Inventory inv = e.getInventory();
		 ItemStack current = e.getCurrentItem();
		 
		 if(current == null) return;
		 if(inv.getName().equals(Menu.getName())){
			 e.setCancelled(true);
			 
			 switch (current.getType()) {
			case WHEAT:
				p.openInventory(Shops.Agriculture);
				break;
			case GOLD_INGOT:
				p.openInventory(Shops.Minerais);
				break;
			case GRASS:
				p.openInventory(Shops.Blocks);
				break;
			case ROTTEN_FLESH:
				p.openInventory(Shops.Loots);
				break;
			case TNT:
				p.openInventory(Shops.Pillage);
				break;
			case MOB_SPAWNER:
				p.openInventory(Shops.Spawners);
				break;
			case COOKED_CHICKEN:
				p.openInventory(Shops.Nourriture);
				break;
			default:
				break;
			}
		 
	    }
	      
		
	}
	
}
