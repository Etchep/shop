package fr.iban.shop.GUIs;

import java.util.Arrays;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import fr.iban.shop.Main;
import fr.iban.shop.Main.ShopType;
import fr.iban.shop.data.PlayerData;
import fr.iban.shop.data.ShopData;

public class ConfirmInventory implements Listener{

	public static HashMap<Integer, ShopData> ShopMap = Main.ShopMap;
	public static HashMap<String, PlayerData> PlayerMap = Main.PlayerMap;

	public static ItemStack getItem(Material material, String name, int quantity, int damage, String desc, boolean enchanted){
		ItemStack it = new ItemStack(material, quantity,  (short) damage);
		ItemMeta itM = it.getItemMeta();
		if(name != null )itM.setDisplayName(name);
		if(desc != null) itM.setLore(Arrays.asList(desc));
		if(enchanted == true){
			itM.addEnchant(Enchantment.DURABILITY, 1, true);
		}
		it.setItemMeta(itM);
		return it;
	}
	
	public static int checkAmountOf(int id, Player p){
		ShopData data = ShopMap.get(id);
		int amount = 0;
		for(ItemStack item : p.getInventory().getContents()){
			if(item != null){
				if(item.getType().equals(data.getMaterial())){
					amount = amount + item.getAmount();
				}
			}
		}
		return amount;
	}


	public static void openInv(int type, int id, Player p){
		ShopData data = ShopMap.get(id);
		String t = null;
		double prix = 1;
		if(type == 1){
			t = "Vendre" ;
			prix = data.getValue() * data.getChange();
			prix = (double)((int)(prix*100))/100;
		}
		if(type == 0){
			t = "Acheter" ;
			prix = data.getValue() * 10 * data.getChange();
			prix = (double)((int)(prix*100))/100;
		}
		Inventory inv = Bukkit.createInventory(null, 54, "�d" + t + " �7| �5" + data.getMaterial().toString());
		p.openInventory(inv);
		PlayerMap.put(p.getName(), new PlayerData(id, 1 , type));
		PlayerData  pdata = PlayerMap.get(p.getName());
		if(type == 1){
			inv.setItem(40, getItem(Material.STAINED_GLASS, "�a�lTout vendre", 1, 5, "�e�lTout vendre pour : �6�l " + checkAmountOf(id, p) * prix + "$", false));
		}
		inv.setItem(22, getItem(data.getMaterial(), null, pdata.getAmount(), data.getDamage(), "�d�lPrix : �5�l" + pdata.getAmount() * prix + "$", false));
		inv.setItem(18, getItem(Material.STAINED_GLASS_PANE, "�e�lMettre � 1", 1, 14, null, false));
		inv.setItem(19, getItem(Material.STAINED_GLASS_PANE, "�e�lRetirer 10", 10, 14, null, false));
		inv.setItem(20, getItem(Material.STAINED_GLASS_PANE, "�e�lRetirer 1", 1, 14, null, false));
		inv.setItem(24, getItem(Material.STAINED_GLASS_PANE, "�e�lAjouter 1", 1, 5, null, false));
		inv.setItem(25, getItem(Material.STAINED_GLASS_PANE, "�e�lAjouter 10", 1, 5, null, false));
		inv.setItem(26, getItem(Material.STAINED_GLASS_PANE, "�e�lMettre � 64", 64, 5, null, false));
		inv.setItem(30, getItem(Material.STAINED_GLASS, "�a�lConfirmer", 1, 5, "�e�l" + t +" pour : �6�l " + pdata.getAmount() * prix + "$", false));
		inv.setItem(32, getItem(Material.STAINED_GLASS, "�c�lAnnuler", 1, 14, null ,false));
	}

	public static void updateInv(Player p, Inventory inv){
		PlayerData  pdata = PlayerMap.get(p.getName());
		ShopData data = ShopMap.get(pdata.getCurrentItem());
		int type = pdata.getType();
		String t = null;
		double prix = 1;
		if(type == 1){
			t = "Vendre" ;
			prix = data.getValue() * data.getChange();
			prix = (double)((int)(prix*100))/100;
		}
		if(type == 0){
			t = "Acheter" ;
			prix = data.getValue() * 10 * data.getChange();
			prix = (double)((int)(prix*100))/100;
		}
		inv.setItem(22, getItem(data.getMaterial(), null, pdata.getAmount(), data.getDamage(), "�d�lPrix : �5�l" + pdata.getAmount() * prix + "$", false));
		inv.setItem(18, getItem(Material.STAINED_GLASS_PANE, "�e�lMettre � 1", 1, 5, null, false));
		inv.setItem(19, getItem(Material.STAINED_GLASS_PANE, "�e�lRetirer 10", 10, 5, null, false));
		inv.setItem(20, getItem(Material.STAINED_GLASS_PANE, "�e�lRetirer 1", 1, 5, null, false));
		inv.setItem(24, getItem(Material.STAINED_GLASS_PANE, "�e�lAjouter 1", 1, 5, null, false));
		inv.setItem(25, getItem(Material.STAINED_GLASS_PANE, "�e�lAjouter 10", 1, 5, null, false));
		inv.setItem(26, getItem(Material.STAINED_GLASS_PANE, "�e�lMettre � 64", 64, 5, null, false));
		inv.setItem(30, getItem(Material.STAINED_GLASS, "�a�lConfirmer", 1, 5, "�e�l" + t +" pour : �6�l " + pdata.getAmount() * prix + "$", false));
		inv.setItem(32, getItem(Material.STAINED_GLASS, "�c�lAnnuler", 1, 14, null ,false));
	}
	


	public static void close(Player p){
		PlayerMap.remove(p.getName());
	}

	@EventHandler
	public void onClick(InventoryClickEvent e){
		Player p = (Player)e.getWhoClicked();
		ItemStack current = e.getCurrentItem();
		if(current == null) return;
		if(PlayerMap.containsKey(p.getName())){
			
			switch(e.getSlot()){
				case 18:
					PlayerMap.get(p.getName()).setAmount(1);
					updateInv(p, e.getInventory());
					e.setCancelled(true);
					break;
				case 19:
					PlayerMap.get(p.getName()).setAmount(PlayerMap.get(p.getName()).getAmount() - 10);
					updateInv(p, e.getInventory());
					e.setCancelled(true);
					break;
				case 20:
					PlayerMap.get(p.getName()).setAmount(PlayerMap.get(p.getName()).getAmount() - 1);
					updateInv(p, e.getInventory());
					e.setCancelled(true);
					break;
				case 24:
					PlayerMap.get(p.getName()).setAmount(PlayerMap.get(p.getName()).getAmount() + 1);
					updateInv(p, e.getInventory());
					e.setCancelled(true);
					break;
				case 25:
					PlayerMap.get(p.getName()).setAmount(PlayerMap.get(p.getName()).getAmount() + 10);
					updateInv(p, e.getInventory());
					e.setCancelled(true);
					break;
				case 26:
					PlayerMap.get(p.getName()).setAmount(64);
					updateInv(p, e.getInventory());
					e.setCancelled(true);
					break;
				case 30:
					PlayerData  pdata = PlayerMap.get(p.getName());
					ShopData data = ShopMap.get(pdata.getCurrentItem());
					int type = pdata.getType();
					double prix = 1;
					if(type == 1){
						if(data.getType().equals(ShopType.ITEM)){
							prix = data.getValue() * pdata.getAmount() * data.getChange();
							prix = (double)((int)(prix*100))/100;
							if(p.getInventory().containsAtLeast(new ItemStack(data.getMaterial(), 1, (short)data.getDamage()), pdata.getAmount())){
								p.getInventory().removeItem(new ItemStack(data.getMaterial(), pdata.getAmount(), (short)data.getDamage()));
								Main.getEcon().depositPlayer(p, prix);
								data.setVentes(data.getVentes() + pdata.getAmount());
								updateInv(p, e.getInventory());
								p.sendMessage("�aVous avez vendu " + pdata.getAmount() + " " + data.getMaterial().toString() + " pour " + prix  +"$.");
							}else{
								p.sendMessage("�c[ECHEC] Vous n'avez pas " + pdata.getAmount() + " " + data.getMaterial().toString() + " dans votre inventaire.");
							}
						}
					}
					if(type == 0){
						if(data.getType().equals(ShopType.ITEM)){
							prix = data.getValue() * 10  * pdata.getAmount() * data.getChange();
							prix = (double)((int)(prix*100))/100;
							if(Main.getEcon().getBalance(p) >= prix){
								if(isInventoryFull(p)){
									p.sendMessage("�c[ECHEC] Vous n'avez pas de place dans votre inventaire.");
								}else{
									Main.getEcon().withdrawPlayer(p, prix);
									p.getInventory().addItem(new ItemStack(data.getMaterial(), pdata.getAmount(), (short) data.getDamage()));
									data.setAchats(data.getAchats() + pdata.getAmount());
									updateInv(p, e.getInventory());
									p.sendMessage("�aVous avez achet� " + pdata.getAmount() + " " + data.getMaterial().toString() + " pour " + prix  +"$.");
								}
							}else{
								p.sendMessage("�c[ECHEC] Vous n'avez pas suffisament d'argent pour acheter ceci.");
							}
						}
						if(data.getType().equals(ShopType.SPAWNER)){
							prix = data.getValue() * 10  * pdata.getAmount() * data.getChange();
							prix = (double)((int)(prix*100))/100;
							if(Main.getEcon().getBalance(p) >= prix){
								if(isInventoryFull(p)){
									p.sendMessage("�c[ECHEC] Vous n'avez pas de place dans votre inventaire.");
								}else{
									Main.getEcon().withdrawPlayer(p, prix);
									Bukkit.dispatchCommand(Bukkit.getConsoleSender(), data.getExtras().getCommand() + " " + p.getName() + " " + data.getExtras().getMob() + " " + pdata.getAmount());
									data.setAchats(data.getAchats() + pdata.getAmount());
									updateInv(p, e.getInventory());
									p.sendMessage("�aVous avez achet� " + pdata.getAmount() + " " + data.getMaterial().toString() + " pour " + prix  +"$.");
								}
							}else{
								p.sendMessage("�c[ECHEC] Vous n'avez pas suffisament d'argent pour acheter ceci.");
							}
						}
					}
					e.setCancelled(true);
					break;
				case 32:
					p.openInventory(Menu.Menu);
					e.setCancelled(true);
					break;
				case 40:
					PlayerData  pdata1 = PlayerMap.get(p.getName());
					ShopData data1 = ShopMap.get(pdata1.getCurrentItem());
					int type1 = pdata1.getType();
					if(type1 == 1){
						if(data1.getType().equals(ShopType.ITEM)){
							double prix1 = data1.getValue() * data1.getChange();
							prix = (double)((int)(prix1*100))/100;
							int amount = 0;
							for(ItemStack item : p.getInventory().getContents()){
								if(item != null){
									if(item.getType().equals(data1.getMaterial())){
										amount = amount + item.getAmount();
										p.getInventory().remove(item);
									}
								}
							}
							if(amount > 0){
								prix = prix*amount;
								Main.getEcon().depositPlayer(p, prix);
								p.sendMessage("�aVous avez vendu " + amount + " " + data1.getMaterial().toString() + " pour " + prix + "$ ." );
							}else{
								p.sendMessage("�cVous ne possedez aucun " + data1.getMaterial().toString() + ".");
							}
						}
					}
					e.setCancelled(true);
					break;
				default:
					e.setCancelled(true);
					break;
			}
		}
	}

	@EventHandler
	public void onClose(InventoryCloseEvent e){
		Player p = (Player) e.getPlayer();
		if(PlayerMap.containsKey(p.getName())){
			close(p);
		}
	}
	
	public boolean isInventoryFull(Player p)
	{
	    return p.getInventory().firstEmpty() == -1;
	}
}

