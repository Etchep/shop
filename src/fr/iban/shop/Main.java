package fr.iban.shop;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import fr.iban.shop.GUIs.ConfirmInventory;
import fr.iban.shop.GUIs.Menu;
import fr.iban.shop.GUIs.Shops;
import fr.iban.shop.data.ExtraData;
import fr.iban.shop.data.PlayerData;
import fr.iban.shop.data.ShopData;
import net.milkbowl.vault.economy.Economy;

public class Main extends JavaPlugin {
	
	public static Economy econ = null;
	public static HashMap<String, PlayerData> PlayerMap;
	public static HashMap<Integer, ShopData> ShopMap;
	
	public static enum ShopType{
		ITEM,COMMAND,SPAWNER
	}
	
	public void onEnable(){
		super.onEnable();
		PlayerMap = new HashMap<String, PlayerData>();
		ShopMap = new HashMap<Integer, ShopData>();
		setShopData();
		Bukkit.getServer().getPluginManager().registerEvents(new Menu(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new Shops(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new ConfirmInventory(), this);
		getCommand("shop").setExecutor(new Commands());
		
		setupEconomy();
		if (!setupEconomy()) {
			getLogger().severe(
					String.format("[%s] - Disabled due to no Vault dependency found!", getDescription().getName(), ShopType.ITEM));
			getServer().getPluginManager().disablePlugin(this);
			return;
		}
	}
	
	public void onDisable(){
		super.onDisable();
	}
	
	private boolean setupEconomy() {
		if (getServer().getPluginManager().getPlugin("Vault") == null) {
			return false;
		}
		RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
		if (rsp == null) {
			return false;
		}
		econ = rsp.getProvider();
		return econ != null;
	}

	public static Economy getEcon() {
		return econ;
	}

	public void setShopData(){
		//id - Categorie - slot - Material - Damage - prix de vente - Type
		ShopMap.put(1, new ShopData(1, "Agriculture", 1, Material.MELON, 0, 1, ShopType.ITEM));
		ShopMap.put(2, new ShopData(2, "Agriculture", 2, Material.PUMPKIN, 0, 4, ShopType.ITEM));
		ShopMap.put(3, new ShopData(3, "Agriculture", 3, Material.CACTUS, 0, 3, ShopType.ITEM));
		ShopMap.put(4, new ShopData(4, "Agriculture", 4, Material.NETHER_STALK, 0, 8, ShopType.ITEM));
		ShopMap.put(5, new ShopData(5, "Agriculture", 5, Material.POTATO_ITEM, 0, 8, ShopType.ITEM));
		ShopMap.put(6, new ShopData(6, "Agriculture", 6, Material.CARROT_ITEM, 0, 8, ShopType.ITEM));
		ShopMap.put(7, new ShopData(7, "Agriculture", 7, Material.WHEAT, 0, 10, ShopType.ITEM));
		
		ShopMap.put(8, new ShopData(8, "Agriculture", 10, Material.SUGAR_CANE, 0, 4, ShopType.ITEM));
		ShopMap.put(9, new ShopData(9, "Agriculture", 11, Material.MELON_SEEDS, 0, 1, ShopType.ITEM));
		ShopMap.put(10, new ShopData(10, "Agriculture", 12, Material.PUMPKIN_SEEDS, 0, 1, ShopType.ITEM));
		ShopMap.put(11, new ShopData(11, "Agriculture", 13, Material.SEEDS, 0, 1, ShopType.ITEM));
		ShopMap.put(12, new ShopData(12, "Agriculture", 14, Material.INK_SACK, 3, 5, ShopType.ITEM));
		ShopMap.put(13, new ShopData(13, "Agriculture", 15, Material.APPLE, 0, 10, ShopType.ITEM));
		ShopMap.put(14, new ShopData(14, "Agriculture", 16, Material.SAPLING, 0, 5, ShopType.ITEM));
		
		ShopMap.put(15, new ShopData(15, "Agriculture", 19, Material.SAPLING, 1, 5, ShopType.ITEM));
		ShopMap.put(16, new ShopData(16, "Agriculture", 20, Material.SAPLING, 2, 5, ShopType.ITEM));
		ShopMap.put(17, new ShopData(17, "Agriculture", 21, Material.SAPLING, 3, 5, ShopType.ITEM));
		ShopMap.put(18, new ShopData(18, "Agriculture", 22, Material.SAPLING, 4, 5, ShopType.ITEM));
		ShopMap.put(19, new ShopData(19, "Agriculture", 23, Material.SAPLING, 5, 5, ShopType.ITEM));
		ShopMap.put(20, new ShopData(20, "Agriculture", 24, Material.CHORUS_FRUIT_POPPED, 0, 3, ShopType.ITEM));
		ShopMap.put(21, new ShopData(21, "Agriculture", 25, Material.BEETROOT, 0, 5, ShopType.ITEM));
		
		ShopMap.put(22, new ShopData(22, "Agriculture", 30, Material.BEETROOT_SEEDS, 0, 1, ShopType.ITEM));
		ShopMap.put(84, new ShopData(84, "Agriculture", 31, Material.BROWN_MUSHROOM, 0, 1, ShopType.ITEM));
		ShopMap.put(85, new ShopData(85, "Agriculture", 32, Material.RED_MUSHROOM, 0, 1, ShopType.ITEM));
		
		
		ShopMap.put(23, new ShopData(23, "Blocks", 1, Material.COBBLESTONE, 0, 1, ShopType.ITEM));
		ShopMap.put(24, new ShopData(24, "Blocks", 2, Material.STONE, 0, 1, ShopType.ITEM));
		ShopMap.put(25, new ShopData(25, "Blocks", 3, Material.DIRT, 0, 1, ShopType.ITEM));
		ShopMap.put(26, new ShopData(26, "Blocks", 4, Material.GRASS, 0, 3, ShopType.ITEM));
		ShopMap.put(27, new ShopData(27, "Blocks", 5, Material.SAND, 0, 3, ShopType.ITEM));
		ShopMap.put(28, new ShopData(28, "Blocks", 6, Material.GRAVEL, 0, 3, ShopType.ITEM));
		ShopMap.put(29, new ShopData(29, "Blocks", 7, Material.OBSIDIAN, 0, 20, ShopType.ITEM));
		ShopMap.put(30, new ShopData(30, "Blocks", 10, Material.WOOL, 0, 3, ShopType.ITEM));
		ShopMap.put(31, new ShopData(31, "Blocks", 11, Material.ENDER_STONE, 0, 30, ShopType.ITEM));
		ShopMap.put(32, new ShopData(32, "Blocks", 12, Material.NETHER_BRICK, 0, 3, ShopType.ITEM));
		ShopMap.put(33, new ShopData(33, "Blocks", 13, Material.NETHERRACK, 0, 1, ShopType.ITEM));
		ShopMap.put(34, new ShopData(34, "Blocks", 14, Material.SOUL_SAND, 0, 3, ShopType.ITEM));
		ShopMap.put(35, new ShopData(35, "Blocks", 15, Material.SPONGE, 0, 10, ShopType.ITEM));
		ShopMap.put(36, new ShopData(36, "Blocks", 16, Material.MYCEL, 0, 8, ShopType.ITEM));
		ShopMap.put(37, new ShopData(37, "Blocks", 19, Material.LOG, 0, 3, ShopType.ITEM));
		ShopMap.put(38, new ShopData(38, "Blocks", 20, Material.LOG, 1, 3, ShopType.ITEM));
		ShopMap.put(39, new ShopData(39, "Blocks", 21, Material.LOG, 2, 3, ShopType.ITEM));
		ShopMap.put(40, new ShopData(40, "Blocks", 22, Material.LOG, 3, 3, ShopType.ITEM));
		ShopMap.put(41, new ShopData(41, "Blocks", 23, Material.LOG_2, 0, 3, ShopType.ITEM));
		ShopMap.put(42, new ShopData(42, "Blocks", 24, Material.LOG_2, 1	, 3, ShopType.ITEM));
		ShopMap.put(43, new ShopData(43, "Blocks", 25, Material.BRICK, 0, 3, ShopType.ITEM));
		ShopMap.put(44, new ShopData(44, "Blocks", 28, Material.PURPUR_BLOCK, 0, 3, ShopType.ITEM));
		ShopMap.put(45, new ShopData(45, "Blocks", 29, Material.SAND, 1, 3, ShopType.ITEM));
		ShopMap.put(46, new ShopData(46, "Blocks", 30, Material.PACKED_ICE, 0, 5, ShopType.ITEM));
		ShopMap.put(47, new ShopData(47, "Blocks", 31, Material.ICE, 0, 3, ShopType.ITEM));
		ShopMap.put(48, new ShopData(48, "Blocks", 32, Material.GLOWSTONE, 0, 3, ShopType.ITEM));
		ShopMap.put(49, new ShopData(49, "Blocks", 33, Material.SANDSTONE, 0, 3, ShopType.ITEM));
		ShopMap.put(50, new ShopData(50, "Blocks", 34, Material.PRISMARINE, 0, 3, ShopType.ITEM));
		ShopMap.put(51, new ShopData(51, "Blocks", 37, Material.PRISMARINE, 1, 3, ShopType.ITEM));
		ShopMap.put(52, new ShopData(52, "Blocks", 38, Material.PRISMARINE, 2, 3, ShopType.ITEM));
		ShopMap.put(53, new ShopData(53, "Blocks", 39, Material.STONE, 1, 3, ShopType.ITEM));
		ShopMap.put(54, new ShopData(54, "Blocks", 40, Material.STONE, 3, 3, ShopType.ITEM));
		ShopMap.put(55, new ShopData(55, "Blocks", 41, Material.STONE, 5, 3, ShopType.ITEM));
		ShopMap.put(56, new ShopData(56, "Blocks", 42, Material.HARD_CLAY, 0, 3, ShopType.ITEM));
		ShopMap.put(57, new ShopData(57, "Blocks", 43, Material.SNOW_BLOCK, 0, 3, ShopType.ITEM));
		
		ShopMap.put(58, new ShopData(58, "Minerais", 10, Material.COAL, 0, 3, ShopType.ITEM));
		ShopMap.put(59, new ShopData(59, "Minerais", 11, Material.COAL_BLOCK, 0, 27, ShopType.ITEM));
		ShopMap.put(60, new ShopData(60, "Minerais", 12, Material.COAL_ORE, 0, 3, ShopType.ITEM));
		ShopMap.put(62, new ShopData(62, "Minerais", 14, Material.IRON_INGOT, 0, 5, ShopType.ITEM));
		ShopMap.put(63, new ShopData(63, "Minerais", 15, Material.IRON_BLOCK, 0, 45, ShopType.ITEM));
		ShopMap.put(64, new ShopData(64, "Minerais", 16, Material.IRON_ORE, 0, 5, ShopType.ITEM));

		ShopMap.put(65, new ShopData(65, "Minerais", 19, Material.GOLD_INGOT, 0, 8, ShopType.ITEM));
		ShopMap.put(66, new ShopData(66, "Minerais", 20, Material.GOLD_BLOCK, 0, 72, ShopType.ITEM));
		ShopMap.put(67, new ShopData(67, "Minerais", 21, Material.GOLD_ORE, 0, 8, ShopType.ITEM));
		ShopMap.put(69, new ShopData(69, "Minerais", 23, Material.REDSTONE, 0, 3, ShopType.ITEM));
		ShopMap.put(70, new ShopData(70, "Minerais", 24, Material.REDSTONE_BLOCK, 0, 27, ShopType.ITEM));
		ShopMap.put(71, new ShopData(71, "Minerais", 25, Material.REDSTONE_ORE, 0, 3, ShopType.ITEM));
		
		ShopMap.put(72, new ShopData(72, "Minerais", 28, Material.INK_SACK, 4, 4, ShopType.ITEM));
		ShopMap.put(73, new ShopData(73, "Minerais", 29, Material.LAPIS_BLOCK, 0, 36, ShopType.ITEM));
		ShopMap.put(74, new ShopData(74, "Minerais", 30, Material.LAPIS_ORE, 0, 4, ShopType.ITEM));
		ShopMap.put(75, new ShopData(75, "Minerais", 32, Material.DIAMOND, 0, 9, ShopType.ITEM));
		ShopMap.put(76, new ShopData(76, "Minerais", 33, Material.DIAMOND_BLOCK, 0, 90, ShopType.ITEM));
		ShopMap.put(77, new ShopData(77, "Minerais", 34, Material.DIAMOND_ORE, 0, 9, ShopType.ITEM));
		
		ShopMap.put(78, new ShopData(78, "Minerais", 1, Material.QUARTZ, 0, 2, ShopType.ITEM));
		ShopMap.put(79, new ShopData(79, "Minerais", 2, Material.QUARTZ_BLOCK, 0, 18, ShopType.ITEM));
		ShopMap.put(80, new ShopData(80, "Minerais", 3, Material.QUARTZ_ORE, 0, 2, ShopType.ITEM));
		ShopMap.put(81, new ShopData(81, "Minerais", 5, Material.EMERALD, 0, 10, ShopType.ITEM));
		ShopMap.put(82, new ShopData(82, "Minerais", 6, Material.EMERALD_BLOCK, 0, 100, ShopType.ITEM));
		ShopMap.put(83, new ShopData(83, "Minerais", 7, Material.EMERALD_ORE, 0, 10, ShopType.ITEM));
		
		ShopMap.put(86, new ShopData(86, "Spawners", 1, Material.MOB_SPAWNER, 0, 10000, ShopType.SPAWNER));
		ShopMap.get(86).setExtras(new ExtraData(null, "ss give", "Zombie", "Zombie"));
		ShopMap.put(87, new ShopData(87, "Spawners", 2, Material.MOB_SPAWNER, 0, 15000, ShopType.SPAWNER));
		ShopMap.get(87).setExtras(new ExtraData(null, "ss give", "Skeleton", "Squelette"));
		ShopMap.put(88, new ShopData(88, "Spawners", 3, Material.MOB_SPAWNER, 0, 20000, ShopType.SPAWNER));
		ShopMap.get(88).setExtras(new ExtraData(null, "ss give", "Blaze", "Blaze"));
		ShopMap.put(89, new ShopData(89, "Spawners", 4, Material.MOB_SPAWNER, 0, 15000, ShopType.SPAWNER));
		ShopMap.get(89).setExtras(new ExtraData(null, "ss give", "Spider", "Arraign�e"));
		ShopMap.put(90, new ShopData(90, "Spawners", 5, Material.MOB_SPAWNER, 0, 15000, ShopType.SPAWNER));
		ShopMap.get(90).setExtras(new ExtraData(null, "ss give", "Enderman", "Enderman"));
		ShopMap.put(91, new ShopData(91, "Spawners", 6, Material.MOB_SPAWNER, 0, 100000, ShopType.SPAWNER));
		ShopMap.get(91).setExtras(new ExtraData(null, "ss give", "Pigman", "Cochon zombie"));
		ShopMap.put(92, new ShopData(92, "Spawners", 7, Material.MOB_SPAWNER, 0, 15000, ShopType.SPAWNER));
		ShopMap.get(92).setExtras(new ExtraData(null, "ss give", "Creeper", "Creeper"));
		
		ShopMap.put(93, new ShopData(93, "Loots", 1, Material.ROTTEN_FLESH, 0, 3, ShopType.ITEM));
		ShopMap.put(94, new ShopData(94, "Loots", 2, Material.BONE, 0, 3, ShopType.ITEM));
		ShopMap.put(95, new ShopData(95, "Loots", 3, Material.SULPHUR, 0, 3, ShopType.ITEM));
		ShopMap.put(96, new ShopData(96, "Loots", 4, Material.STRING, 0, 3, ShopType.ITEM));
		ShopMap.put(97, new ShopData(97, "Loots", 5, Material.SPIDER_EYE, 0, 3, ShopType.ITEM));
		ShopMap.put(98, new ShopData(98, "Loots", 6, Material.ENDER_PEARL, 0, 15, ShopType.ITEM));
		ShopMap.put(99, new ShopData(99, "Loots", 7, Material.SLIME_BALL, 0, 10, ShopType.ITEM));
		
		ShopMap.put(100, new ShopData(100, "Loots", 10, Material.FEATHER, 0, 5, ShopType.ITEM));
		ShopMap.put(101, new ShopData(101, "Loots", 11, Material.INK_SACK, 0, 3, ShopType.ITEM));
		ShopMap.put(102, new ShopData(102, "Loots", 12, Material.MAGMA_CREAM, 0, 3, ShopType.ITEM));
		ShopMap.put(103, new ShopData(103, "Loots", 13, Material.ARROW, 0, 3, ShopType.ITEM));
		ShopMap.put(104, new ShopData(104, "Loots", 14, Material.BLAZE_ROD, 0, 5, ShopType.ITEM));
		ShopMap.put(105, new ShopData(105, "Loots", 15, Material.LEATHER, 0, 3, ShopType.ITEM));
		ShopMap.put(106, new ShopData(106, "Loots", 16, Material.RABBIT_HIDE, 0, 3, ShopType.ITEM));
		
		ShopMap.put(107, new ShopData(107, "Loots", 22, Material.SHULKER_SHELL, 0, 20, ShopType.ITEM));
		
		ShopMap.put(108, new ShopData(108, "Pillage", 21, Material.TNT, 0, 5, ShopType.ITEM));
		ShopMap.put(109, new ShopData(109, "Pillage", 22, Material.MONSTER_EGG, 50, 5000, ShopType.ITEM));
		ShopMap.put(110, new ShopData(110, "Pillage", 23, Material.FLINT_AND_STEEL, 0, 5, ShopType.ITEM));
		
		ShopMap.put(111, new ShopData(111, "Nourriture", 1, Material.COOKED_BEEF, 0, 5, ShopType.ITEM));
		ShopMap.put(112, new ShopData(112, "Nourriture", 2, Material.COOKED_CHICKEN, 0, 5, ShopType.ITEM));
		ShopMap.put(113, new ShopData(113, "Nourriture", 3, Material.COOKED_MUTTON, 0, 5, ShopType.ITEM));
		ShopMap.put(114, new ShopData(114, "Nourriture", 4, Material.COOKED_RABBIT, 0, 5, ShopType.ITEM));
		ShopMap.put(115, new ShopData(115, "Nourriture", 5, Material.COOKED_FISH, 0, 5, ShopType.ITEM));
		ShopMap.put(116, new ShopData(116, "Nourriture", 6, Material.GOLDEN_APPLE, 0, 50, ShopType.ITEM));
		ShopMap.put(117, new ShopData(117, "Nourriture", 7, Material.GOLDEN_CARROT, 0, 10, ShopType.ITEM));
		
		
	}


}